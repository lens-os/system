// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

/// A handle to an object in the kernel. Corresponds to a file descriptor; the term "handle" is used due to their common use for things that are not files.
pub struct Handle {
    pub descriptor: i32,
}

impl Drop for Handle {
    fn drop(&mut self) {
        lens_syscalls::close(self.descriptor);
    }
}

/*impl Clone for Handle {
  fn clone(self: &Handle) {
    // TODO: Use dup.
  }
}*/
